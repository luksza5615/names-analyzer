# Overview
The project is a collection of solutions for several Java challenges mmade fully for practise.


## names-analyzer
Console analyzer of most popular names in USA in 2011-2016, based on 
https://catalog.data.gov/dataset/most-popular-baby-names-by-sex-and-mothers-ethnic-group-new-york-city-8c742 

Application shows following statictics: 
* top x most popular names overall 
* top x most popular names of a specified gender 
* top x most popular characters that names are begun with

Main focus:
* file reading
* collections usage
* lambda expressions
* method references

## pesel-checker
National number identification code validator. It checks e.g. number corectness based on checksum, person sex.

Checker implemented for
* Polish pesel https://pl.wikipedia.org/wiki/PESEL
* Italian fiscal code https://en.wikipedia.org/wiki/Italian_fiscal_code_card

## hospital-manager
Simple console hospital staff manager
