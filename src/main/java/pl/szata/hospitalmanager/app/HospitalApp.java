package pl.szata.hospitalmanager.app;

public class HospitalApp {
	public static final String APP_NAME = "Hospital Application";
	
	public static void main(String[] args) {
		System.out.println(APP_NAME);
		HospitalControl hc = new HospitalControl();
		hc.controlLoop();
	}
}
