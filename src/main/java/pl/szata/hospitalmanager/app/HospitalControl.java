package pl.szata.hospitalmanager.app;

import pl.szata.hospitalmanager.data.Hospital;
import pl.szata.hospitalmanager.data.Person;
import pl.szata.hospitalmanager.utils.DataReader;
import pl.szata.hospitalmanager.utils.FileManager;
import pl.szata.hospitalmanager.utils.HospitalUtils;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;

public class HospitalControl {
    private DataReader dr;
    private Hospital hosp;
    private FileManager fileManager;

    public HospitalControl() {
        dr = new DataReader();
        fileManager = new FileManager();
        try {
            hosp = fileManager.readHospitalFromFile();
            System.out.println("Wczytano szpital z pliku");
        } catch (ClassNotFoundException | IOException e) {
            hosp = new Hospital();
            System.out.println("Utworzono nowy szpital");
        }
    }

    public void controlLoop() {
        Option option = null;
        while ((option != Option.EXIT)) {
            printOptions();
            try {
                option = Option.createFromInt(dr.getInt());

                switch (option) {
                    case ADD_PATIENT:
                        addPatient();
                        break;
                    case ADD_DOCTOR:
                        addDoctor();
                        break;
                    case ADD_NURSE:
                        addNurse();
                        break;
                    case PRINT_PATIENTS:
                        HospitalUtils.printPatients(hosp);
                        break;
                    case PRINT_DOCTORS:
                        HospitalUtils.printDoctors(hosp);
                        break;
                    case PRINT_NURSES:
                        HospitalUtils.printNurses(hosp);
                        break;
                    case PRINT_HOSPITAL:
                        HospitalUtils.printHospital(hosp);
                        break;
                    case PRINT_STAFF:
                        HospitalUtils.printStaff(hosp);
                        break;
                    case REMOVE_PATIENT:
                        removePatient();
                        break;
                    case REMOVE_DOCTOR:
                        removeDoctor();
                        break;
                    case REMOVE_NURSE:
                        removeNurse();
                        break;
                    case EXIT:
                        exit();
                        break;
                }
            } catch (InputMismatchException e) {
                System.out.println("Wprowadzono niepoprawne dane, publikacji nie dodano");
            } catch (NumberFormatException | NoSuchElementException e) {
                System.out.println("Wybrana opcja nie istnieje, wybierz ponownie:");
            }
        }

        dr.scannerClose();
    }

    private void printOptions() {
        System.out.println("Wybierz jedną z następujących opcji: ");
        for (Option opt : Option.values()) {
            System.out.println(opt);
        }
    }

    private void addPatient() {
        Person patient = dr.readAndCreatePatient();
        hosp.addToHospital(patient);
    }

    private void addNurse() {
        Person nurse = dr.readAndCreateNurse();
        hosp.addToHospital(nurse);
    }

    private void addDoctor() {
        Person doctor = dr.readAndCreateDoctor();
        hosp.addToHospital(doctor);
    }

    private void removePatient() {
        Person patient = dr.readAndRemovePatient();
        hosp.removeFromHospital(patient);

    }

    private void removeDoctor() {
        Person doctor = dr.readAndRemoveDoctor();
        hosp.removeFromHospital(doctor);
    }

    private void removeNurse() {
        Person nurse = dr.readAndRemoveNurse();
        hosp.removeFromHospital(nurse);
    }

    private void exit() {
        fileManager.writeHospitalToFile(hosp);
    }

    public enum Option {
        EXIT(0, "wyjście w programu"),
        ADD_PATIENT(1, "dodaj pacjenta do szpitala"),
        ADD_DOCTOR(2, "dodaj lekarza do szpitala"),
        ADD_NURSE(3, "dodaj pielęgniarkę do szpitala"),
        PRINT_PATIENTS(4, "wyświetl wszystkich pacjentów w szpitalu"),
        PRINT_DOCTORS(5, "wyświetl wszystkich lekarzy w szpitalu"),
        PRINT_NURSES(6, "wyświetl wszystkie pielęgniarki w szpitalu"),
        PRINT_HOSPITAL(7, "wyświetl wszytkie osoby w szpitalu"),
        PRINT_STAFF(8, "wyświetl personel szpitala"),
        REMOVE_PATIENT(9, "wypisz pacjenta ze szpitala"),
        REMOVE_DOCTOR(10, "usuń lekarza ze szpitala"),
        REMOVE_NURSE(11, "usuń pielęgniarkę ze szpitala");

        private int value;
        private String description;

        Option(int value, String desc) {
            this.value = value;
            this.description = desc;
        }

        @Override
        public String toString() {
            return value + " - " + description;
        }

        public static Option createFromInt(int option) throws NoSuchElementException {
            Option result = null;
            try {
                result = Option.values()[option];
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new NoSuchElementException("Brak elementu o wskazanym ID");
            }

            return result;
        }
    }
}

