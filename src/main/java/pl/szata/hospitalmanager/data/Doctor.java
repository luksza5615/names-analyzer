package pl.szata.hospitalmanager.data;

import java.io.Serializable;

public class Doctor extends Staff implements Serializable {
	private static final long serialVersionUID = 5252946504370335854L;
	
	private int bonus;

	public int getBonus() {
		return bonus;
	}

	public void setBonus(int bonus) {
		this.bonus = bonus;
	}

	public Doctor(String firstName, String lastName, int salary, int bonus) {
		super(firstName, lastName, salary);
		setBonus(bonus);
	}
	
	public Doctor(String firstName, String lastName) {
		super(firstName, lastName);
	}
	
	public Doctor() {
		super();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Doctor other = (Doctor) obj;
		return bonus == other.bonus;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(super.toString());
		sb.append(" " + bonus);
		return sb.toString();
	}
}
