package pl.szata.hospitalmanager.data;

import java.io.Serializable;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

public class Hospital implements Serializable {
	private static final long serialVersionUID = -9088360302264895689L;

	private Map<String, Person> allPeople;
	private Map<String, Person> allStaff;
	private Map<String, Person> allPatients;
	private Map<String, Person> allNurses;
	private Map<String, Person> allDoctors;

	public Hospital() {
		allPeople = new HashMap<>();
		allStaff = new HashMap<>();
		allPatients = new HashMap<>();
		allNurses = new HashMap<>();
		allDoctors = new HashMap<>();
	}

	public Map<String, Person> getAllPeople() {
		return allPeople;
	}

	public Map<String, Person> getAllStaff() {
		return allStaff;
	}

	public Map<String, Person> getAllPatients() {
		return allPatients;
	}

	public Map<String, Person> getAllNurses() {
		return allNurses;
	}

	public Map<String, Person> getAllDoctors() {
		return allDoctors;
	}

	public int getStaffNumber() {
		return allStaff.size();
	}

	public int getPatientsNumber() {
		return allPatients.size();
	}

	public int getDoctorsNumber() {
		return allDoctors.size();
	}

	public int getNursesNumber() {
		return allNurses.size();
	}

	// metoda dodająca do odpowiednich zbiorów - cały szpital, personel,
	// pielęgniarki etc.
	public void addToHospital(Person p) {
		allPeople.put(p.getLastName(), p);

		if (p instanceof Nurse) {
			allNurses.put(p.getLastName(), p);
		}

		if (p instanceof Staff) {
			allStaff.put(p.getLastName(), p);
		}

		if (p instanceof Doctor) {
			allDoctors.put(p.getLastName(), p);
		}

		if (p instanceof Patient) {
			allPatients.put(p.getLastName(), p);
		}

	}

	// testowa klasa sprawdzajaca usuwanie lekarza
	public void removeDoctorFromHospital(Person d) throws NoSuchElementException {
		try {
			if (allDoctors.containsValue(d)) {
				System.out.println("Znaleziono takiego typa");
				System.out.println("Przekazany klucz: " + d.getLastName());
				boolean zawiera = allDoctors.containsKey(d.getLastName());
				System.out.println("Zawiera klucz?: " + zawiera);
				allDoctors.remove(d.getLastName());
			} else {
				throw new NoSuchElementException();
			}
		} catch (NoSuchElementException e){
			System.err.println("Nie znaleziono obiektu");
			throw e;
		}
	}

	public void removeFromHospital(Person p) {

		if (allPeople.containsValue(p)) {
			allPeople.remove(p.getLastName());
		} 
	
		if (allStaff.containsValue(p)) {
			allStaff.remove(p.getLastName());
		}
		if (allNurses.containsValue(p)) {
			allNurses.remove(p.getLastName());
		}
		if (allDoctors.containsValue(p)) {
			allDoctors.remove(p.getLastName());
		}
		if (allPatients.containsValue(p)) {
			allPatients.remove(p.getLastName());
		}

	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Person p : allPeople.values()) {
			sb.append(p);
			sb.append("/n");
		}
		return sb.toString();
	}

	// komparator segregujący po nazwisku
	public static class LastNameComparator implements Comparator<Person> {
		@Override
		public int compare(Person p1, Person p2) {

			if (p1 == null & p2 == null) {
				return 0;
			}

			if (p1 == null) {
				return 1;
			}

			if (p2 == null) {
				return -1;
			}

			return p1.getLastName().compareTo(p2.getLastName());
		}
	}

}
