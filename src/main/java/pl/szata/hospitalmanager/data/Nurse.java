package pl.szata.hospitalmanager.data;

public class Nurse extends Staff {
	private static final long serialVersionUID = -5842983770646711635L;
	
	private int overtime;

	public int getOvertime() {
		return overtime;
	}

	public void setOvertime(int overtime) {
		this.overtime = overtime;
	}

	public Nurse(String firstName, String lastName, int salary, int overtime) {
		super(firstName, lastName, salary);
		setOvertime(overtime);
	}
	
	public Nurse(String firstName, String lastName) {
		setFirstName(firstName);
		setLastName(lastName);
	}

	public Nurse() {
		super();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Nurse other = (Nurse) obj;
		return overtime == other.overtime;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(super.toString());
		sb.append(" " + overtime);
		return sb.toString();
	}

}