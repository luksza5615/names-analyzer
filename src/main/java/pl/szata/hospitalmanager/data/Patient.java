package pl.szata.hospitalmanager.data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Patient extends Person {
	private static final long serialVersionUID = 1455715939599014603L;
	
	private List<LocalDate> admissionHistory;
	private String pesel;

	public List<LocalDate> getAdmissionHistory() {
		return admissionHistory;
	}

	public void setAdmissionHistory(List<LocalDate> admissionHistory) {
		this.admissionHistory = admissionHistory;
	}
	
	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public Patient(String firstName, String lastName, String pesel) {
		super(firstName, lastName);
		setPesel(pesel);
		setAdmissionHistory(new ArrayList<>());
	}
	
	public Patient() {
		super();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Patient other = (Patient) obj;
		if (pesel == null) {
			return other.pesel == null;
		} else return pesel.equals(other.pesel);
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(super.toString());
		sb.append(" " + pesel);
		return sb.toString();
	}
}
