package pl.szata.hospitalmanager.data;

import java.io.Serializable;

public class Person implements Serializable, Comparable<Person> {
	private static final long serialVersionUID = -3210205728713239941L;
	private String firstName;
	private String lastName;

	public Person() {
	}

	public Person(String firstName, String lastName) {
		setFirstName(firstName);
		setLastName(lastName);
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			return other.lastName == null;
		} else return lastName.equals(other.lastName);
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(firstName + " ");
		sb.append(lastName);
		return sb.toString();
	}
	
	@Override
	public int compareTo(Person p) {
		return lastName.compareTo(p.getLastName());
	}

}
