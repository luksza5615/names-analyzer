package pl.szata.hospitalmanager.data;

import java.io.Serializable;

public class Staff extends Person implements Serializable, Comparable<Person> { 
	static final long serialVersionUID = -5559618347339889024L;
	
	private int salary;

	public Staff(String firstName, String lastName) {
		super(firstName, lastName);
	}

	public Staff(String firstName, String lastName, int salary) {
		super(firstName, lastName);
		setSalary(salary);
	}

	public Staff() {
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Staff other = (Staff) obj;
		return salary == other.salary;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(super.toString());
		sb.append(" ");
		sb.append(salary);
		return sb.toString();
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
