package pl.szata.hospitalmanager.utils;

import pl.szata.hospitalmanager.data.Doctor;
import pl.szata.hospitalmanager.data.Nurse;
import pl.szata.hospitalmanager.data.Patient;

import java.util.InputMismatchException;
import java.util.Scanner;

public class DataReader {
	public Scanner sc;

	public DataReader() {
		sc = new Scanner(System.in);
	}

	public void scannerClose() {
		sc.close();
	}

	public int getInt() throws NumberFormatException {
		int number;
		try {
			number = sc.nextInt();
		} catch (InputMismatchException e) {
			throw new NumberFormatException("Liczba wprowadzona w niepoprawnej formie");
		} finally {
		sc.nextLine();
		}
		
		return number;
	}

	public Doctor readAndCreateDoctor() throws InputMismatchException {
		System.out.println("Podaj imię lekarza");
		String firstName = sc.nextLine();
		System.out.println("Podaj nazwisko lekarza");
		String lastName = sc.nextLine();
		System.out.println("Podaj wynagrodzenie lekarza");
		int salary = 0;
		int bonus = 0;
		try {
			salary = sc.nextInt();
			sc.nextLine();
			System.out.println("Podaj premię lekarza");
			bonus = sc.nextInt();
			sc.nextLine();
		} catch (InputMismatchException e) {
			sc.nextLine();
			throw e;
		}
		return new Doctor(firstName, lastName, salary, bonus);
	}
	
	public Patient readAndCreatePatient() {
		System.out.println("Podaj imię pacjenta");
		String firstName = sc.nextLine();
		System.out.println("Podaj nazwisko pacjenta");
		String lastName = sc.nextLine();
		System.out.println("Podaj PESEL pacjenta");
		String pesel = sc.nextLine();

		return new Patient(firstName, lastName, pesel);
	}

	public Nurse readAndCreateNurse() {
		System.out.println("Podaj imię pielęgniarki");
		String firstName = sc.nextLine();
		System.out.println("Podaj nazwisko pielęgniarki");
		String lastName = sc.nextLine();
		System.out.println("Podaj wynagrodzenie pielęgniarki");
		int salary = 0;
		int overtime = 0;
		
		try {
			salary = sc.nextInt();
			sc.nextLine();
			System.out.println("Podaj nadgodziny pielęgniarki");
			overtime = sc.nextInt();
			sc.nextLine();
		} catch (InputMismatchException e) {
			sc.nextLine();
			throw e;
		}
		
		return new Nurse(firstName, lastName, salary, overtime);
	}
	
	public Patient readAndRemovePatient() {
		System.out.println("Podaj imię pacjenta, którego chcesz wypisać");
		String firstName = sc.nextLine();
		System.out.println("Podaj nazwisko pacjenta, którego chcesz wypisać");
		String lastName = sc.nextLine();
		System.out.println("Podaj PESEL pacjenta, którego chcesz wypisać");
		String pesel = sc.nextLine();

		return new Patient(firstName, lastName, pesel);
	}
	
	public Doctor readAndRemoveDoctor() {
		System.out.println("Podaj imię lekarza, którego chcesz usunąć");
		String firstName = sc.nextLine();
		System.out.println("Podaj nazwisko lekarza, którego chcesz usunąć");
		String lastName = sc.nextLine();
		System.out.println("Podaj wynagrodzenie");
		int salary = sc.nextInt();
		sc.nextLine();
		System.out.println("Podaj bonus");
		int bonus = sc.nextInt();
		sc.nextLine();
		Doctor doc = new Doctor(firstName, lastName, salary, bonus);
		System.out.println(doc);

		return doc;
	}
	
	public Nurse readAndRemoveNurse() {
		System.out.println("Podaj imię pielęgniarki, którą chcesz usunąć");
		String firstName = sc.nextLine();
		System.out.println("Podaj nazwisko pielęgniarki, którą chcesz usunąć");
		String lastName = sc.nextLine();
		System.out.println("Podaj wynagrodzenie pielęgniarki");
		int salary = sc.nextInt();
		sc.nextLine();
		System.out.println("Podaj nadgodziny");
		int overtime = sc.nextInt();
		sc.nextLine();

		return new Nurse(firstName, lastName, salary, overtime);
	}
}
