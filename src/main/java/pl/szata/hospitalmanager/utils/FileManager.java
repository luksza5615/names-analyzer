package pl.szata.hospitalmanager.utils;

import pl.szata.hospitalmanager.data.Hospital;

import java.io.*;

public class FileManager {
	public static final String FILE_NAME = "src/main/resources/hospitalmanager/HospitalApp.txt";
	File file = new File(FILE_NAME);

	public void writeHospitalToFile(Hospital hosp)  {
		try(
			FileOutputStream fos = new FileOutputStream(FILE_NAME);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
		) {
			oos.writeObject(hosp);
		} catch (FileNotFoundException e) {
			System.out.println("Nie znaleziono pliku: " + FILE_NAME);
		} catch (IOException e) {
			System.out.println("Błąd podczas zapisu pliku: " + FILE_NAME);
		}
	}
	
	public Hospital readHospitalFromFile() throws FileNotFoundException, IOException, ClassNotFoundException {
		Hospital hosp = null;
		try(
				FileInputStream fos = new FileInputStream(FILE_NAME);
				ObjectInputStream ois = new ObjectInputStream(fos);
				FileReader reader = new FileReader(FILE_NAME);
				BufferedReader b = new BufferedReader(reader);
		) {
				hosp = (Hospital)ois.readObject();
		} catch (FileNotFoundException e) {
			System.out.println("Nie znaleziono pliku: " + FILE_NAME);
			throw e;
		} catch (IOException e) {
			System.out.println("Błąd podczas odczytu pliku: " + FILE_NAME);
			throw e;
		} catch (ClassNotFoundException e) {
			System.out.println("Nieprawidłowy format pliku");
			throw e;
		}

		return hosp;
	}
}
