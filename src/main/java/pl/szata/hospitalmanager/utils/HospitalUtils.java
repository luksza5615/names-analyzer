package pl.szata.hospitalmanager.utils;

import pl.szata.hospitalmanager.data.*;

public class HospitalUtils {
	
	public static void printHospital(Hospital hosp) {
		printStaffInfo(hosp, Person.class);
	}
	
	public static void printStaff(Hospital hosp) {
		printStaffInfo(hosp, Staff.class);
	}
	
	public static void printNurses(Hospital hosp) {
		printStaffInfo(hosp, Nurse.class);
	}
	
	public static void printDoctors(Hospital hosp) {
		printStaffInfo(hosp, Doctor.class);
	}
	
	public static void printPatients(Hospital hosp) {
		printStaffInfo(hosp, Patient.class);
	}
	
	private static void printStaffInfo(Hospital hosp, Class cl) {
		long count = hosp.getAllPeople()
				.values()
				.stream()
				.filter(cl::isInstance)
				.sorted(new Hospital.LastNameComparator())
				.peek(System.out::println)
				.count();
		
		if (count == 0) {
			System.out.println("Nie ma żadnej osoby o typie " + cl.getSimpleName());
		}
	}
}
