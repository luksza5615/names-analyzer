package pl.szata.namesanalyzer;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileReader {

    //Reads all names from the file
    public static List<Name> readAllNames() {
        List<Name> names = new ArrayList<>();
        String filePath = new File("src/main/resources/namesanalyzer/Popular_Baby_Names.csv").getAbsolutePath();

        try {
            BufferedReader reader = new BufferedReader(new java.io.FileReader(filePath));
            reader.readLine(); //to ignore first line with header
            String line;

            while ((line = reader.readLine()) != null) {
                String[] nameLine = line.split(",");
                Gender gender = Gender.of(nameLine[1]);
                String literalName = nameLine[3].substring(0, 1).toUpperCase() + nameLine[3].substring(1).toLowerCase();
                int count = Integer.parseInt(nameLine[4]);
                Name name = new Name(literalName, gender, count);
                names.add(name);
            }

            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return names;
    }

}
