package pl.szata.namesanalyzer;

public enum Gender {
    MALE, FEMALE;

    public static Gender of(String input) {
        if (input.equals("MALE")) {
            return MALE;
        } else {
            return FEMALE;
        }
    }
}
