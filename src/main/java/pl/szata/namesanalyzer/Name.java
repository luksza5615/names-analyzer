package pl.szata.namesanalyzer;

import java.util.Objects;

public class Name {
    private String literal;
    private Gender gender;
    private int count;

    public Name(String literal, Gender gender, int count) {
        this.literal = literal;
        this.gender = gender;
        this.count = count;
    }

    public String getLiteral() {
        return literal;
    }

    public void setLiteral(String literal) {
        this.literal = literal;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Name name = (Name) o;
        return Objects.equals(literal, name.literal) &&
                gender == name.gender;
    }

    @Override
    public int hashCode() {
        return Objects.hash(literal, gender);
    }
}
