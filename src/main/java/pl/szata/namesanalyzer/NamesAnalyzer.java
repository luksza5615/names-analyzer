package pl.szata.namesanalyzer;

import java.util.*;
import java.util.stream.Collectors;

class NamesAnalyzer {

    //returns top n most popular names
    public static void getTopN(int n) {
        Map<String, Integer> map = sumNames();
        System.out.println(n + " most popular names");
        map.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).limit(n).forEach(System.out::println);
    }

    //returns top n most popular names in specified gender
    public static void getTopNofGender(int n, Gender gender) {
        System.out.println("\n" + n + " most popular names by gender");
        Map<String, Integer> map = sumNamesByGender(gender);
        map.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).limit(n).forEach(System.out::println);
    }

    //count names beginning with each character
    public static void sumNamesByFirstCharacterAndGetTopN(int n) {
        List<String> names = FileReader.readAllNames().stream().map(Name::getLiteral).distinct().collect(Collectors.toList()); //list of all distinct names
        Map<Character, Integer> charsMap = new HashMap<>();

        for (int i = 65; i <= 90; i++) {
            int count = 0;
            char character = (char) i;
            charsMap.put(character, count);

            for (String name : names) {
                char firstCharInName = name.charAt(0);

                if (firstCharInName == i) {
                    charsMap.put(firstCharInName, charsMap.get(firstCharInName) + 1);
                }
            }
        }

        System.out.println("\n" + "Top " + n + " characters with sum of names beginning with the character");
        charsMap.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).limit(n).forEach(System.out::println);
    }

    ////creates pairs: name out of specified gender-sum of counts
    private static Map<String, Integer> sumNamesByGender(Gender gender) {
        List<Name> names = FileReader.readAllNames();
        Map<String, Integer> namesMap = new HashMap<>();

        for (Name name : names) {
            Gender nameGender = name.getGender();
            String nameLiteral = name.getLiteral();

            if (nameGender.equals(gender) && namesMap.keySet().contains(nameLiteral)) { //if name already exists in the map
                namesMap.put(nameLiteral, namesMap.get(nameLiteral) + name.getCount()); //replaces map entry with new value

            } else if (nameGender.equals(gender) && !namesMap.keySet().contains(nameLiteral)) { //if name not exist in the map yet
                namesMap.put(name.getLiteral(), name.getCount());
            }
        }

        return namesMap;
    }

    //creates pairs: name-sum of counts
    private static Map<String, Integer> sumNames() {
        List<Name> names = FileReader.readAllNames();
        Map<String, Integer> namesMap = new HashMap<>();

        for (Name name : names) {
            if (namesMap.keySet().contains((name.getLiteral()))) {
                namesMap.put(name.getLiteral(), namesMap.get(name.getLiteral()) + name.getCount());
            } else {
                namesMap.put(name.getLiteral(), name.getCount());
            }
        }
        return namesMap;
    }

}
