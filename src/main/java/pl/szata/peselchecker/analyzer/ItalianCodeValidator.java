package pl.szata.peselchecker.analyzer;

import pl.szata.peselchecker.exception.IdentificationCodeInvalidException;
import pl.szata.peselchecker.exception.InvalidIdentificationCodeFormat;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ItalianCodeValidator {

    //sprawdzenie ważności kodu fiscalCode na podstawie ostatniego znaku
    public static boolean validateFiscalCode(String fiscalCode) throws IdentificationCodeInvalidException {
        //3 pomocnicze mapy na których bazuje liczenie checksumy
        HashMap<Character, Integer> oddChars = fillOddCharsMap();
        HashMap<Character, Integer> evenChars = fillEvenCharsMap();
        HashMap<Integer, Character> remainders = fillRemindersMap();

        //utworzenie dwóch tablic - ze znakami na miejscach nieparzystych i parzystych
        char[] fiscalCodeOddNumbers = new char[8];
        char[] fiscalCodeEvenNumbers = new char[8];
        int oddArrayIndex = 0;
        int evenArrayIndex = 1;

        //wyciągnięcie wartości z fiscalCode do odpowiednich tablic
        for (int i = 0; i < 15; i++) {
            if (i % 2 == 0) {
                fiscalCodeOddNumbers[oddArrayIndex] = fiscalCode.charAt(i);
                oddArrayIndex = oddArrayIndex + 1;
            } else {
                fiscalCodeEvenNumbers[evenArrayIndex - 1] = fiscalCode.charAt(i);
                evenArrayIndex = evenArrayIndex + 1;
            }
        }

        int oddSum = 0;
        int evenSum = 0;
        oddArrayIndex = 0;
        evenArrayIndex = 0;

        for (int i = 0; i < 15; i++) {
            if (i % 2 == 0) {
                oddSum = oddSum + oddChars.get(fiscalCodeOddNumbers[oddArrayIndex]);
                oddArrayIndex = oddArrayIndex + 1;
            } else {
                evenSum = evenSum + evenChars.get(fiscalCodeEvenNumbers[evenArrayIndex]);
                evenArrayIndex = evenArrayIndex + 1;
            }
        }

        int totalSum = oddSum + evenSum;

        if (fiscalCode.charAt(15) == remainders.get(totalSum % 26)) {
            return true;
        } else {
            throw new IdentificationCodeInvalidException();
        }
    }

    //sprawdzenie formatu kodu fiscalCode
    public static boolean validateFiscalCodeFormat(String fiscalCode) throws InvalidIdentificationCodeFormat {
        //MLLSNT82P65Z404U
        String regex = "\\w{6}\\d{2}\\w\\d{2}\\w\\d{2}\\w";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(fiscalCode);

        if (matcher.find()) {
            return true;
        } else {
            throw new InvalidIdentificationCodeFormat();
        }
    }


    private static HashMap<Character, Integer> fillOddCharsMap() {
        HashMap<Character, Integer> oddChars = new HashMap<>();
        oddChars.put('0', 1);
        oddChars.put('1', 0);
        oddChars.put('2', 5);
        oddChars.put('3', 7);
        oddChars.put('4', 9);
        oddChars.put('5', 13);
        oddChars.put('6', 15);
        oddChars.put('7', 17);
        oddChars.put('8', 19);
        oddChars.put('9', 21);
        oddChars.put('A', 1);
        oddChars.put('B', 0);
        oddChars.put('C', 5);
        oddChars.put('D', 7);
        oddChars.put('E', 9);
        oddChars.put('F', 13);
        oddChars.put('G', 15);
        oddChars.put('H', 17);
        oddChars.put('I', 19);
        oddChars.put('J', 21);
        oddChars.put('K', 2);
        oddChars.put('L', 4);
        oddChars.put('M', 18);
        oddChars.put('N', 20);
        oddChars.put('O', 11);
        oddChars.put('P', 3);
        oddChars.put('Q', 6);
        oddChars.put('R', 8);
        oddChars.put('S', 12);
        oddChars.put('T', 14);
        oddChars.put('U', 16);
        oddChars.put('V', 10);
        oddChars.put('W', 22);
        oddChars.put('X', 25);
        oddChars.put('Y', 24);
        oddChars.put('Z', 23);

        return oddChars;
    }

    private static HashMap<Character, Integer> fillEvenCharsMap() {

        HashMap<Character, Integer> evenChars = new HashMap<>();
        evenChars.put('0', 0);
        evenChars.put('1', 1);
        evenChars.put('2', 2);
        evenChars.put('3', 3);
        evenChars.put('4', 4);
        evenChars.put('5', 5);
        evenChars.put('6', 6);
        evenChars.put('7', 7);
        evenChars.put('8', 8);
        evenChars.put('9', 9);
        evenChars.put('A', 0);
        evenChars.put('B', 1);
        evenChars.put('C', 2);
        evenChars.put('D', 3);
        evenChars.put('E', 4);
        evenChars.put('F', 5);
        evenChars.put('G', 6);
        evenChars.put('H', 7);
        evenChars.put('I', 8);
        evenChars.put('J', 9);
        evenChars.put('K', 10);
        evenChars.put('L', 11);
        evenChars.put('M', 12);
        evenChars.put('N', 13);
        evenChars.put('O', 14);
        evenChars.put('P', 15);
        evenChars.put('Q', 16);
        evenChars.put('R', 17);
        evenChars.put('S', 18);
        evenChars.put('T', 19);
        evenChars.put('U', 20);
        evenChars.put('V', 21);
        evenChars.put('W', 22);
        evenChars.put('X', 23);
        evenChars.put('Y', 24);
        evenChars.put('Z', 25);

        return evenChars;
    }

    private static HashMap<Integer, Character> fillRemindersMap() {

        HashMap<Integer, Character> remainders = new HashMap<>();
        remainders.put(0, 'A');
        remainders.put(1, 'B');
        remainders.put(2, 'C');
        remainders.put(3, 'D');
        remainders.put(4, 'E');
        remainders.put(5, 'F');
        remainders.put(6, 'G');
        remainders.put(7, 'H');
        remainders.put(8, 'I');
        remainders.put(9, 'J');
        remainders.put(10, 'K');
        remainders.put(11, 'L');
        remainders.put(12, 'M');
        remainders.put(13, 'N');
        remainders.put(14, 'O');
        remainders.put(15, 'P');
        remainders.put(16, 'Q');
        remainders.put(17, 'R');
        remainders.put(18, 'S');
        remainders.put(19, 'T');
        remainders.put(20, 'U');
        remainders.put(21, 'V');
        remainders.put(22, 'W');
        remainders.put(23, 'X');
        remainders.put(24, 'Y');
        remainders.put(25, 'Z');

        return remainders;
    }

}
