package pl.szata.peselchecker.analyzer;

import pl.szata.peselchecker.exception.IdentificationCodeInvalidException;

public class PolishCodeValidator {

    //Pesel validator (pesel stored as long)
    public static boolean validatePesel(long pesel) throws IdentificationCodeInvalidException {
        int[] peselDigits = new int[10];
        int[] weights = {9, 7, 3, 1, 9, 7, 3, 1, 9, 7};
        long divider = 10_000_000_000L;
        int checksum = 0;

        for (int i = 0; i < 10; i++) {
            peselDigits[i] = (int) ((pesel / divider) % 10);
            checksum += peselDigits[i] * weights[i];
            divider = divider / 10;
        }

        long lastPeselDigit = pesel % 10;
        if (checksum % 10 == lastPeselDigit) {
            return true;
        } else {
            throw new IdentificationCodeInvalidException();
        }
    }

    //Pesel validator (pesel stored as string)
    public static boolean validatePesel(String pesel) throws IdentificationCodeInvalidException {
        int[] peselDigits = new int[10];
        int[] weights = {9, 7, 3, 1, 9, 7, 3, 1, 9, 7};
        int checksum = 0;

        for (int i = 0; i < 10; i++) {
            peselDigits[i] = Character.getNumericValue(pesel.charAt(i));
            checksum += peselDigits[i] * weights[i];
        }

        long lastPeselDigit = Character.getNumericValue(pesel.charAt(10));
        if (checksum % 10 == lastPeselDigit) {
            return true;
        } else {
            throw new IdentificationCodeInvalidException();
        }
    }

}
