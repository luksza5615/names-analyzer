package pl.szata.peselchecker.data;

import pl.szata.peselchecker.exception.NameTooShortException;

public abstract class Citizen {
    private String firstName;
    private String lastName;
    private int yearOfBirth;

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Citizen(String firstName, String lastName) throws NameTooShortException {

        if (firstName.length() < 1) {
            throw new NameTooShortException();
        }
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String toString() {
        return firstName + " " + lastName + " " + yearOfBirth;
    }

}
