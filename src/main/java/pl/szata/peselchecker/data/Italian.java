package pl.szata.peselchecker.data;

import pl.szata.peselchecker.analyzer.ItalianCodeValidator;
import pl.szata.peselchecker.exception.IdentificationCodeInvalidException;
import pl.szata.peselchecker.exception.InvalidIdentificationCodeFormat;
import pl.szata.peselchecker.exception.NameTooShortException;
import pl.szata.peselchecker.util.Month;
import pl.szata.peselchecker.util.Sex;

import java.util.Arrays;
import java.util.List;

public class Italian extends Citizen {
    private String fiscalCode;
    private Sex sex;
    private int yearOfBirth;
    private String monthOfBirth;
    private byte dayOfBirth;

    //creates an italian citizen with fiscal code validation
    public Italian(String firstName, String lastName, String fiscalCode) throws NameTooShortException, IdentificationCodeInvalidException, InvalidIdentificationCodeFormat {
        super(firstName, lastName);

        if (!ItalianCodeValidator.validateFiscalCodeFormat(fiscalCode)) {
            throw new InvalidIdentificationCodeFormat();
        }

        setYearOfBirth(fiscalCode);
        setMonthOfBirth(fiscalCode);
        setDayOfBirth(fiscalCode);
        setSex(fiscalCode);

        if (ItalianCodeValidator.validateFiscalCode(fiscalCode)) {
            this.fiscalCode = fiscalCode;
        } else {
            throw new IdentificationCodeInvalidException();
        }
    }

    public void setLastName(String lastName) {
        super.setLastName(lastName);
    }

    //take year of birth from fiscalCode
    private void setYearOfBirth(String fiscalCode) {
        int birthDigits = Integer.parseInt(fiscalCode.substring(6, 8));

        if (birthDigits < 19) {
            this.yearOfBirth = 2000 + birthDigits;
        } else {
            this.yearOfBirth = 1900 + birthDigits;
        }
    }

    //take month of birth from fiscalCode
    private void setMonthOfBirth(String fiscalCode) {
        char monthChar = fiscalCode.charAt(8);
        List<Month> months = Arrays.asList(Month.values());

        int i = 0;
        while (monthChar != months.get(i).getMonthChar()) {
            i++;
        }
        this.monthOfBirth = months.get(i).getPolishDesc();
    }

    //take date of birth from fiscalCode
    private void setDayOfBirth(String fiscalCode) {
        byte dayOfBirth = Byte.parseByte(fiscalCode.substring(9, 11));

        if (dayOfBirth > 40) {
            this.dayOfBirth = (byte) (dayOfBirth - 40);
        } else {
            this.dayOfBirth = dayOfBirth;
        }
    }

    private void setSex(String fiscalCode) {
        byte dayOfBirthOriginal = Byte.parseByte(fiscalCode.substring(9, 11));
        if (dayOfBirthOriginal > 40) {
            this.sex = Sex.FEMALE;
        } else {
            this.sex = Sex.MALE;
        }
    }

    public byte getDayOfBirth() {
        return this.dayOfBirth;
    }

    public String toString() {
        return this.getFirstName() + " " + this.getLastName() + " " + this.yearOfBirth + " " + this.dayOfBirth + " " + this.monthOfBirth + " " + this.sex.getDescription();
    }
}
