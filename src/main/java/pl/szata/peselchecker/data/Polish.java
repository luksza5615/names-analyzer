package pl.szata.peselchecker.data;

import pl.szata.peselchecker.analyzer.PolishCodeValidator;
import pl.szata.peselchecker.exception.IdentificationCodeInvalidException;
import pl.szata.peselchecker.exception.NameTooShortException;
import pl.szata.peselchecker.util.Sex;

public class Polish extends Citizen {
    private String pesel;
    private long peselLong;
    private Sex sex;

    /******** Pesel as Srtring************8*/
    public Polish(String firstName, String lastName, String pesel) throws IdentificationCodeInvalidException, NameTooShortException {
        super(firstName, lastName);
        setBirthOfYear(pesel, this);

        if (PolishCodeValidator.validatePesel(pesel)) {
            this.pesel = pesel;
        }

        if (checkSex(pesel)) {
            this.sex = Sex.FEMALE;
        } else {
            this.sex = Sex.MALE;
        }
    }

    private static void setBirthOfYear(String pesel, Polish polish) {
        // example "85011212345"
        String year = pesel.substring(0, 2); // "85"
        char century = pesel.charAt(2); // '0'
        if (century > '1') {
            polish.setYearOfBirth(2000 + Integer.valueOf(year));
        } else {
            polish.setYearOfBirth(1900 + Integer.valueOf(year));
        }
    }

    /*******Differet version: PESEL stored as a long***********/
    public Polish(String firstName, String lastName, long pesel) throws IdentificationCodeInvalidException, NameTooShortException {
        super(firstName, lastName);
        setBirthOfYear(pesel, this);

        if (PolishCodeValidator.validatePesel(pesel)) {
            this.peselLong = pesel;
        }

        if (checkSex(pesel)) {
            this.sex = Sex.FEMALE;
        } else {
            this.sex = Sex.MALE;
        }
    }

    private static void setBirthOfYear(long pesel, Polish polish) {
        // example "85011212345"
        byte year = (byte) (pesel / 1_000_000_000L);
        byte century = (byte) (pesel / 1_000_000_00L % 10);
        if (century > 1) {
            polish.setYearOfBirth(2000 + year);
        } else {
            polish.setYearOfBirth(1900 + year);
        }
    }

    private static boolean checkSex(long pesel) {
        int sexDigit = (int) ((pesel / 10) % 10);
        return sexDigit % 2 == 0;
    }

    private static boolean checkSex(String pesel) {
        int sexDigit = Character.getNumericValue(pesel.charAt(9));
        return sexDigit % 2 == 0;

    }

    public String toString() {
        if (pesel == null) {
            return super.toString() + " " + peselLong + " " + sex.getDescription();
        } else {
            return super.toString() + " " + pesel + " " + sex.getDescription();
        }
    }
}