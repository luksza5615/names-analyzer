package pl.szata.peselchecker.main;

import pl.szata.peselchecker.data.Italian;
import pl.szata.peselchecker.data.Polish;
import pl.szata.peselchecker.exception.IdentificationCodeInvalidException;
import pl.szata.peselchecker.exception.InvalidIdentificationCodeFormat;
import pl.szata.peselchecker.exception.NameTooShortException;


public class Main {
    public static void main(String[] args) {

        try {
            Polish janKowalski = new Polish("Jan", "Kowalski", "91073012373");
            Polish annaKowalska = new Polish("Anna", "Kowalska", "91113002667");
            Polish marianNowak = new Polish("Marian", "Nowak", 91073012373L);
            Italian marianoItaliano = new Italian("Mariano", "Italiano", "MRTMTT25D09F205Z");
            Italian samanthaMiller = new Italian("Samantha", "Miller", "MLLSNT82P65Z404U");

            System.out.println(janKowalski);
            System.out.println(annaKowalska);
            System.out.println(marianNowak);
            System.out.println(marianoItaliano);
            System.out.println(samanthaMiller);

        } catch (IdentificationCodeInvalidException e)  {
            System.out.println("Pesel jest nieprawidłowy");
        } catch (NameTooShortException e) {
            System.out.println("Podane imię jest za krótkie");
        } catch (InvalidIdentificationCodeFormat e) {
            System.out.println("Podany kod identyfikacyjny jest nieprawidłowy");
        }
    }
}
