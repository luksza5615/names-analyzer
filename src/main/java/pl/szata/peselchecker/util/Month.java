package pl.szata.peselchecker.util;

public enum Month {
    JANUARY('A', "January", "Styczeń"),
    FEBRUARY('B', "February", "Luty"),
    MARCH('C', "March", "Marzec"),
    APRIL('D', "April", "Kwiecień"),
    MAY('E', "May", "Maj"),
    JUNE('H', "June", "Czerwiec"),
    JULY('L', "July", "Lipiec"),
    AUGUST('M', "August", "Sierpień"),
    SEPTEMBER('P', "September", "Wrzesień"),
    OCTOBER('R', "October", "Październik"),
    NOVEMBER('S', "November", "Listopad"),
    DECEMBER('T', "December", "Grudzień");

    char monthChar;
    String engDesc;
    String polishDesc;

    public char getMonthChar() {
        return monthChar;
    }

    public String getEngDesc() {
        return engDesc;
    }

    public String getPolishDesc() {
        return polishDesc;
    }

    Month(char monthChar, String engDesc, String polishDesc) {
        this.monthChar = monthChar;
        this.engDesc = engDesc;
        this.polishDesc = polishDesc;
    }
}
