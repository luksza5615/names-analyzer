package pl.szata.peselchecker.util;

public enum Sex {
    MALE("Mężczyzna"),
    FEMALE("Kobieta");

    private String description;

    Sex(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
